package PT2019.demo.DemoProject;




public class MonitorData {
	
	public Data inceput;
	public Data sfarsit;
	public String eticheta;
	
	public MonitorData(Data a,Data b,String e)
	{
		this.inceput=a;
		this.sfarsit=b;
		this.eticheta=e;
	}
	public Data scadere(Data a,Data b)
	{
		
		Data x=new Data(0,0,0,0,0,0);		
		x.an=b.an-a.an;
		x.luna=b.luna-a.luna;
		x.zi=b.zi-a.zi;
		x.ora=b.ora-a.ora;
		x.minute=b.minute-a.minute;
		x.secunde=b.secunde-a.secunde;
		if(x.zi<0)
		{
			x.zi=x.zi+31;
			x.luna--;
		}
		while (x.ora < 0 || x.minute < 0 || x.secunde < 0) {
			if (x.ora < 0) {
				x.ora = x.ora + 24;
				x.zi--;
			}
			if (x.minute < 0) {
				x.minute = x.minute + 60;
				x.ora--;
			}
			if (x.secunde < 0) {
				x.secunde = x.secunde + 60;
				x.minute--;
			}
		}
		return x;
	}
	public Data aduna(Data a,Data b)
	{

		Data x=new Data(0,0,0,0,0,0);		
		x.an=b.an+a.an;
		x.luna=b.luna+a.luna;
		x.zi=b.zi+a.zi;
		x.ora=b.ora+a.ora;
		x.minute=b.minute+a.minute;
		x.secunde=b.secunde+a.secunde;		
		while (x.ora > 60 || x.minute > 60 || x.secunde > 60 || x.luna>12 || x.zi>30) {
			if(x.luna>12)
			{
				x.luna=x.luna-12;
				x.an++;
			}
			if(x.zi>30)
			{
				x.zi=x.zi-30;
				x.luna++;
			}
			if (x.ora > 60) {
				x.ora = x.ora -60;
				x.zi++;
			}
			if (x.minute > 60) {
				x.minute = x.minute - 60;
				x.ora++;
			}
			if (x.secunde > 60) {
				x.secunde = x.secunde - 60;
				x.minute++;
			}
		}
		return x;
	}
	

}
