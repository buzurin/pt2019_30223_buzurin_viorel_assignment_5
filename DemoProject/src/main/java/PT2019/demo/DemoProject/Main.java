package PT2019.demo.DemoProject;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;



public class Main {

	public static MonitorData []lista=new MonitorData[1000];
	public static int k=0;
	public static Map<String, Integer> numarAct()
	{
		int[]act=new int[11];
		String []nu= { "Leaving", "Toileting", "Showering","Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
		for(int i=0;i<11;i++)
			act[i]=0;
		for(int i=0;i<k;i++)
		{
			switch (lista[i].eticheta ) {
			case "Leaving": act[0]++;    break;
			case "Toileting":   act[1]++; 
            break;
			case "Showering": act[2]++;
            break;
			case "Sleeping": act[3]++;
            break;
			case "Breakfast":  act[4]++;
	        break;
			case "Lunch":  act[5]++;
            break;
			case "Dinner":  act[6]++;
            break;
			case "Snack":  act[7]++;
            break;
			case "Spare_Time/TV": act[8]++;
            break;
			case "Grooming":  act[9]++;
            break;
			default: 
            break;
			}
		}
		 Map<String,Integer> map=new HashMap<String,Integer>();  
		 for(int i=0;i<10;i++)
		 {
			 map.put(nu[i],act[i]);
		 }
		return map;
	}
	public static void numarZile()
	{
		int []a=new int[31];
		int []act=new int[31];
		String []nu= { "Leaving", "Toileting", "Showering","Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
		for(int i=0;i<31;i++)
			{
				a[i]=0;
				act[i]=0;
			}
		int zi=lista[0].inceput.zi;
		for(int i=0;i<k;i++)			
		{			
			a[lista[i].inceput.zi]=1;
			a[lista[i].sfarsit.zi]=1;
			switch (lista[i].eticheta ) {
			case "Leaving": act[0]++;    break;
			case "Toileting":   act[1]++; 
            break;
			case "Showering": act[2]++;
            break;
			case "Sleeping": act[3]++;
            break;
			case "Breakfast":  act[4]++;
	        break;
			case "Lunch":  act[5]++;
            break;
			case "Dinner":  act[6]++;
            break;
			case "Snack":  act[7]++;
            break;
			case "Spare_Time/TV": act[8]++;
            break;
			case "Grooming":  act[9]++;
            break;
			default: 
            break;
			}
			if(zi!=lista[i].inceput.zi || i==k-1) {
				System.out.println("Ziua:"+zi);
			for(int j=0;j<10;j++)
			{
				System.out.println(nu[j]+":"+act[j]);
				act[j]=0;
			}
			zi=lista[i].inceput.zi;
			}
		}
		int t=0;
		for(int i=0;i<31;i++)
		{
			if(a[i]!=0)
				t++;
		}
		System.out.println("Numar de zile:"+t);
		
	}
	public static void timpAct()
	{
		Data x=new Data(0,0,0,0,0,0);
		Data[]act=new Data[11];
		String []nu= { "Leaving", "Toileting", "Showering","Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
		for(int i=0;i<11;i++)
			act[i]=new Data(0,0,0,0,0,0);
		for(int j=0;j<k;j++)
		{
			x=lista[j].scadere(lista[j].inceput, lista[j].sfarsit);
			switch (lista[j].eticheta ) {
			case "Leaving": act[0]=lista[j].aduna(x, act[0]);    break;
			case "Toileting":   act[1]=lista[j].aduna(x, act[1]);
            break;
			case "Showering":act[2]=lista[j].aduna(x, act[2]);
            break;
			case "Sleeping":act[3]=lista[j].aduna(x, act[3]);
            break;
			case "Breakfast":  act[4]=lista[j].aduna(x, act[4]);
	        break;
			case "Lunch":  act[5]=lista[j].aduna(x, act[5]);
            break;
			case "Dinner":  act[6]=lista[j].aduna(x, act[6]);
            break;
			case "Snack": act[7]=lista[j].aduna(x, act[7]);
            break;
			case "Spare_Time/TV": act[8]=lista[j].aduna(x, act[8]);
            break;
			case "Grooming":  act[9]=lista[j].aduna(x, act[9]);
            break;
			default: 
            break;
			}
			//System.out.print(lista[j].eticheta+"  ");
			//x.afis();
		}
		
		for(int i=0;i<10;i++)
		{
			System.out.print(nu[i]+" :");
			act[i].afis();
		}
	}
	public static Map<String, Integer> timp5min()
	{
		Data x=new Data(0,0,0,0,0,0);	
		int []act=new int[11];
		String []nu= { "Leaving", "Toileting", "Showering","Sleeping", "Breakfast", "Lunch", "Dinner", "Snack", "Spare_Time/TV", "Grooming"};
		for(int i=0;i<11;i++)
		{			
			act[i]=0;
		}			
		for(int j=0;j<k;j++)
		{
			x=lista[j].scadere(lista[j].inceput, lista[j].sfarsit);
			if(x.an==0 && x.luna==0 && x.zi==0 && x.ora==0 && x.minute<=5) {
			switch (lista[j].eticheta ) {
			case "Leaving": act[0]++;    break;
			case "Toileting":   act[1]++; 
            break;
			case "Showering": act[2]++;
            break;
			case "Sleeping": act[3]++;
            break;
			case "Breakfast":  act[4]++;
	        break;
			case "Lunch":  act[5]++;
            break;
			case "Dinner":  act[6]++;
            break;
			case "Snack":  act[7]++;
            break;
			case "Spare_Time/TV": act[8]++;
            break;
			case "Grooming":  act[9]++;
            break;
			default: 
            break;
			}			
		}
		}		
			Map<String,Integer> map=new HashMap<String,Integer>();  
			 for(int i=0;i<10;i++)
			 {
				 map.put(nu[i],act[i]);
			 }
			return map;		
		
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		FileInputStream f=new FileInputStream("act.txt");
		InputStreamReader fchar=new InputStreamReader(f);
		BufferedReader buf=new BufferedReader(fchar);	
		String linie = null;
		try {
			linie=buf.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		while (linie != null) {
			Data a = null, b = null;
			int an, luna, zi, ora, minute, secunde, j;
			String h = null;			
			StringTokenizer t = new StringTokenizer(linie);
			for (j = 1; j <= 3; j++) {
				if (j != 3) {
					h = t.nextToken();
					an = Integer.parseInt(h.substring(0, 4));
					luna = Integer.parseInt(h.substring(5, 7));
					zi = Integer.parseInt(h.substring(8, 10));
					h = t.nextToken();
					ora = Integer.parseInt(h.substring(0, 2));
					minute = Integer.parseInt(h.substring(3, 5));
					secunde = Integer.parseInt(h.substring(6, 8));
					if (j == 1)
						a = new Data(an, luna, zi, ora, minute, secunde);
					else
						b = new Data(an, luna, zi, ora, minute, secunde);
				} else {
					h = t.nextToken();					
				}
			}
			lista[k]=new MonitorData(a,b,h);
			k++;
			try {
				linie=buf.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		}
		//Main.numarZile();
		//Main.timpAct();
		Map<String, Integer> map=new HashMap<String,Integer>();
		Map<String, Integer> map1=new HashMap<String,Integer>();
		map=Main.numarAct();
		map1=Main.timp5min();
		/*
		map.entrySet().stream().forEach(e ->
				System.out.println("Key : " + e.getKey() + " value : " + e.getValue()));
		map1.entrySet().stream().forEach(e ->
		      System.out.println("Key : " + e.getKey() + " value : " + e.getValue()));*/
		
		Iterator<Entry<String, Integer>> iterator = map.entrySet().iterator();
		Iterator<Entry<String, Integer>> iterator1 = map1.entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry entry = iterator.next();
			Map.Entry entry1 = iterator1.next();			
			int x=(int) entry.getValue();
			int y=(int) entry1.getValue();
			if(x!=0)
			System.out.println("Key : " + entry.getKey() +"  "+  (y*1.0/x)*100+"  %");
		}
			  
	}
		
	}

	


